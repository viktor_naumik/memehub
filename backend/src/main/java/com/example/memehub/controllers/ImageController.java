package com.example.memehub.controllers;

import com.example.memehub.entities.ImageEntity;
import com.example.memehub.security.CurrentUser;
import com.example.memehub.security.UserPrincipal;
import com.example.memehub.services.ImageService;
import com.example.memehub.services.ImageStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;

@PreAuthorize("hasAnyAuthority('USER', 'ADMIN')")
@RestController
@RequestMapping("/api/images")
public class ImageController {
  @Autowired ImageStorageService imageStorageService;
  @Autowired ImageService imageService;

  @GetMapping
  public Page<ImageEntity> getAllImages(
      @RequestParam(defaultValue = "0") Integer pageNo,
      @RequestParam(defaultValue = "10") Integer pageSize,
      @RequestParam(defaultValue = "id") String sortBy) {
    return imageService.getAllImages(pageNo, pageSize, sortBy);
  }

  @PreAuthorize("hasAnyAuthority('USER')")
  @PostMapping
  public ImageEntity uploadImage(
      @RequestParam("image") MultipartFile image,
      @RequestParam(required = false) Long[] tags,
      @ApiIgnore @CurrentUser UserPrincipal user) {
    if (tags == null) tags = new Long[0];
    return imageService.uploadImage(image, tags, user);
  }

  @PreAuthorize("permitAll()")
  @GetMapping("{fileName:.+}")
  public Resource downloadFileFromLocal(
      HttpServletResponse response, @PathVariable String fileName) {
    return imageStorageService.downloadFileFromStorage(fileName, response);
  }

  @PutMapping("assignTagToImage")
  public ImageEntity assignTagToImage(@RequestParam Long imageId, Long tagId) {
    return imageService.assignTagToImage(imageId, tagId);
  }

  @GetMapping("allByTags")
  public Page<ImageEntity> getAllByTags(
      @RequestParam(required = false) Long[] tags,
      @RequestParam(defaultValue = "0") Integer pageNo,
      @RequestParam(defaultValue = "10") Integer pageSize,
      @RequestParam(defaultValue = "id") String sortBy,
      @ApiIgnore @CurrentUser UserPrincipal userPrincipal) {
    if (tags == null) tags = new Long[0];
    return imageService.getAllByTags(tags, pageNo, pageSize, sortBy, userPrincipal);
  }

  @GetMapping("byId/{id}")
  public ImageEntity getImageById(@PathVariable Long id) {
    return imageService.getImageById(id);
  }

  @PutMapping("deleteTagFromImage")
  public ImageEntity deleteTagFromImage(@RequestParam Long imageId, @RequestParam Long tagId) {
    return imageService.deleteTagFromImage(imageId, tagId);
  }
}
