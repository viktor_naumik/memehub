package com.example.memehub.services;

import com.example.memehub.dao.ImageRepository;
import com.example.memehub.dao.TagRepository;
import com.example.memehub.dao.UserRepository;
import com.example.memehub.entities.ImageEntity;
import com.example.memehub.entities.RoleName;
import com.example.memehub.entities.TagEntity;
import com.example.memehub.entities.UserEntity;
import com.example.memehub.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ImageService {
  @Autowired ImageRepository imageRepository;
  @Autowired TagRepository tagRepository;
  @Autowired UserRepository userRepository;
  @Autowired ImageStorageService imageStorageService;

  public Page<ImageEntity> getAllImages(Integer pageNo, Integer pageSize, String sortBy) {
    PageRequest pageRequest = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
    return imageRepository.findAll(pageRequest);
  }

  public ImageEntity assignTagToImage(Long imageId, Long tagId) {
    if (!imageRepository.existsById(imageId))
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Image not found");
    if (!tagRepository.existsById(tagId))
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Tag not found");
    ImageEntity imageEntity = imageRepository.getOne(imageId);
    TagEntity tagEntity = tagRepository.getOne(tagId);
    if (imageEntity.getTags().contains(tagEntity))
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tag already assigned to image");
    imageEntity.getTags().add(tagEntity);
    return imageRepository.saveAndFlush(imageEntity);
  }

  public Page<ImageEntity> getAllByTags(
      Long[] tags, Integer pageNo, Integer pageSize, String sortBy, UserPrincipal userPrincipal) {
    PageRequest pageRequest = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
    List<ImageEntity> entities;
    UserEntity userEntity = userRepository.getOne(userPrincipal.getId());
    if(userEntity.hasRole(RoleName.ADMIN)) {
      entities = imageRepository.findAll();
    } else {
      entities = userEntity.getImages();
    }
    int start = pageNo * pageSize;
    int end = (pageNo + 1) * pageSize;
    List<ImageEntity> result =
        entities
            .parallelStream()
            .filter(imageEntity -> imageEntity.isImageContainsTags(tags))
            .collect(Collectors.toList());
    end = Math.min(end, result.size());

    return new PageImpl<>(result.subList(start, end), pageRequest, result.size());
  }

  public ImageEntity getImageById(Long id) {
    Optional<ImageEntity> imageEntity = imageRepository.findById(id);
    if (!imageEntity.isPresent())
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Image Not found");
    return imageEntity.get();
  }

  public ImageEntity deleteTagFromImage(Long imageId, Long tagId) {
    if (!imageRepository.existsById(imageId))
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Image not found");
    ImageEntity imageEntity = imageRepository.getOne(imageId);
    if (!imageEntity.isImageContainsTag(tagId))
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tag doesn't assigned to image");
    imageEntity.removeTag(tagId);
    return imageRepository.saveAndFlush(imageEntity);
  }

  public ImageEntity uploadImage(MultipartFile file, Long[] tags, UserPrincipal user) {
    String fileDownloadUri = imageStorageService.uploadFileToStorage(file);
    UserEntity userEntity = userRepository.getOne(user.getId());
    ImageEntity image = new ImageEntity(fileDownloadUri);
    image.setUser(userEntity);
    for (Long tag : tags) {
      Optional<TagEntity> tagEntity = tagRepository.findById(tag);
      tagEntity.ifPresent(entity -> image.getTags().add(entity));
    }
    return imageRepository.saveAndFlush(image);
  }
}
