import { hot } from 'react-hot-loader/root';
import React from 'react';
import MainRouter from '../routes/MainRouter.jsx';
import styles from './App.css';

const App = () => {
  return (
    <div className={styles.Container}>
      <MainRouter />
    </div>
  );
};

export default hot(App);
